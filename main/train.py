import networkx as nx
import pickle
import numpy as np
import ipdb
import re
import matplotlib.pyplot as plt
import sys

#创建图谱
def create_g(filename):
    G = nx.DiGraph()
    with open(filename,encoding = 'utf-8') as file:
        lines = file.readlines()
        lines = [line.rstrip() for line in lines]
#nodename为“Mention:...”
    nodetext = [node for node in lines if 'Mention' in node]
    nodes = []
    for i in range(len(nodetext)):
        nodename = re.search('\{(.*)\}', nodetext[i])
        nodes.append((i, nodename.group(1)))
        G.add_node(i, name=nodename.group(1))
#edgename为因果、顺承等
    edgetext = [edge for edge in lines if 'Action' in edge]
    edges = []
    for i in range(len(edgetext)):
        connect = re.findall('[0-9]+', edgetext[i])
        edgename = re.search('\:(.*)\{', edgetext[i]).group(1)
        edgeattr = re.search('\'(.*)\'', edgetext[i])
        if edgeattr == None:
            edgeattr = re.search('\"(.*)\"', edgetext[i])
        edgeattr = edgeattr.group(1)
        # edges.append((int(connect[0]), int(connect[1]), {edgename: edgeattr}))
        edges.append((int(connect[0]), int(connect[1]), edgeattr))
        G.add_edge(int(connect[0]), int(connect[1]), label=edgeattr)
    return G, nodes, edges

#根据设计的公式添加权重
def weights_measure(G, edges, source, target, metric='centrality'):
    if metric == 'centrality':
        scores = nx.betweenness_centrality(G)
        copy = G.copy()
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                all_path = list(nx.all_simple_paths(copy, i, j))
                

        weights = []
        for edge in edges:
            node0 = edge[0]
            node1 = edge[1]
            score = scores[node0]+scores[node1]
            if score == 0:
                weight = sys.MAX
            else: 
                weight = 2/score
            weights.append((node0, node1, weight))
        G.add_weighted_edges_from(weights)
    if metric == 'centrality':
        copy = G.copy()
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                all_path = nx.all_simple_paths(G, i, j)
    return G, weights

def find_all_path(G):
    all = []
    for i in range(len(G.nodes)):
        for j in range(len(G.nodes)):
            try:
                all_path = list(nx.all_simple_paths(G, i, j))
                all = all+all_path
            except (nx.exception.NetworkXNoPath): continue
    return all

def dupli(the_list):
    count = the_list.count # this optimization added courtesy of Sven's comment
    result = [(item, count(item)) for item in set(the_list)]
    result.sort()
    return result

#加载图谱文件
filename = '印度事理.txt'
#设置参数
k = 3
source = 4
target = 35
E1 = []

metric = 'connectivity'
#创建图谱
G, nodes, edges = create_g(filename)
# G_weigh, weights = weights_measure(G, edges, source, target, metric='centrality')

#定义每种价值评估指标
try: 
    all_path = list(nx.all_simple_paths(G, source, target))
except (nx.exception.NetworkXNoPath): 
    print('invalid input of source node and target node')

influence1 = []
influence2 = []
influence3 = []
influence4 = []
# metric == 'centrality':
scores1 = nx.betweenness_centrality(G)
# metric == 'attention':
attention = [3.1, 3.6, 4.7, 3.5, 4.6, 4.9, 3.5, 4.9, 4.5, 3.8, 4.3, 4.6, 4.6, 4.9, 4.1, 4.2, 
3.1, 3.4, 3.9, 4.4, 4.6, 4.5, 5.0, 4.6, 4.8, 4.4, 3.8, 3.7, 3.3, 4.1, 4.6, 4.8, 
4.9, 4.3, 3.1, 3.9, 4.6, 4.5, 4.2, 3.7, 3.1, 4.4, 4.1, 4.0, 3.9, 4.8, 3.0, 3.2]
scores2 = [0] * len(nodes)
for i in range(len(nodes)):
    pre = [pred for pred in G.predecessors(i)]
    if not pre:
        scores2[i] = G.degree[i] * 0.2
    else: 
        scores2[i] = sum(attention[j]/G.degree[j] for j in pre)*0.8 + G.degree[i] * 0.2
# metric == 'connectivity':
connect = [0]*3
# metric == 'scarcity':
all = all_path
alllen = [len(p) for p in all]
allnum = list(set(alllen))
allcount = [0] * max(allnum)
for i in range(1, len(allcount)+1):
    allcount[i-1] = alllen.count(i)
plen = [len(p) for p in all_path]
pnum = list(set(plen))
pcount = [0] * max(pnum)
for i in range(1, len(pcount)+1):
    pcount[i-1] = plen.count(i)

#寻找第一条路径
for p in all_path:
    influence1.append(sum([scores1[i] for i in p]))
    influence2.append(sum([scores2[i] for i in p]))
    for i in range(len(p)-1):
        label = G.get_edge_data(p[i], p[i+1])['label']
        if label == '使得': connect[0] += 1
        elif label == '接着': connect[1] += 1
        else: connect[2] += 1
    influence3.append(sum([a*b for a,b in zip(connect, [0.6, 0.3, 0.1])])*0.8)
    tmp = len(p)-1
    influence4.append(np.log(1+allcount[tmp]/pcount[tmp]))
    
#np,argmax为输出数组中的最大索引值
influence = [a*b*c*d for a,b,c,d in zip(influence1,influence2,influence3,influence4)]
max_ind = np.argmax(influence)
E1.append(all_path[max_ind])

#寻找次路径
cur_base = 0
while (len(E1) < 5):
    cur_base_path = E1[cur_base]
    E2 = []
    for i in range(len(cur_base_path)-1):
        gcopy = G.copy()
        gcopy.remove_edge(cur_base_path[i], cur_base_path[i+1])
        try: 
            part_path = list(nx.all_simple_paths(gcopy, cur_base_path[i], cur_base_path[i+1]))
        except (nx.exception.NetworkXNoPath): continue
        if not part_path: continue

        plen = [len(p) for p in part_path]
        pnum = list(set(plen))
        pcount = [0] * max(pnum)
        for i in range(1, len(pcount)+1):
            pcount[i-1] = plen.count(i)
    
        influence1 = []
        influence2 = []
        influence3 = []
        influence4 = []

        for p in part_path:
            influence1.append(sum([scores1[i] for i in p]))
            influence2.append(sum([scores2[i] for i in p]))
            for i in range(len(p)-1):
                label = G.get_edge_data(p[i], p[i+1])['label']
                if label == '使得': connect[0] += 1
                elif label == '接着': connect[1] += 1
                else: connect[2] += 1
            influence3.append(sum([a*b for a,b in zip(connect, [0.6, 0.3, 0.1])])*0.8)
            tmp = len(p)-1
            influence4.append(np.log(1+allcount[tmp]/pcount[tmp]))
        influence = [a*b*c*d for a,b,c,d in zip(influence1,influence2,influence3,influence4)]
        max_ind = np.argmax(influence)
        if i == len(cur_base_path)-2:
            new_path = cur_base_path[:i] + p
        else:
            new_path = cur_base_path[:i] + p + cur_base_path[i+2:]
        E2.append(new_path)
    E1 = E1+E2
    if len(E1)-1 > cur_base:
        cur_base = cur_base+1
    else:
        print('There is only {} valid paths between source node and target node'.format(len(E1)))

print(E1[:5])#E1为存储top-k条路径的数组，eg:[4,31,33,13,...,24]
#print_path_detail(G, E1[:5])
