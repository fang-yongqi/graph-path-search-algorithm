# -*- coding:cp936 -*-
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.pyplot import plot,savefig
# fname 为 你下载的字体库路径，注意 SourceHanSansSC-Bold.otf 字体的路径
zhfont1 = matplotlib.font_manager.FontProperties(fname="D:\浏览器下载\SourceHanSansSC-Bold.otf")

#读取各个曲线的数据，画图
filename1 = 'C:/Users/fyq/OneDrive/桌面/算法/duibi/meas_sweep_20221006_121715.dat'                       # 文件名
X1,Y1 = [],[]
with open(filename1, 'r') as f:#1
    lines = f.readlines()#2
    for line in lines:#3
        value = [float(s) for s in line.split()]#4
        X1.append(value[0])#5
        Y1.append(value[1])
plt.plot(X1, Y1, label='main')

filename2 = 'C:/Users/fyq/OneDrive/桌面/实验dat文件/实验3/mprofile366.dat'                       # 文件名
X2,Y2 = [],[]
with open(filename2, 'r') as f:#1
    lines = f.readlines()#2
    for line in lines:#3
        alue = [float(s) for s in line.split()]#4
        X2.append(alue[0])#5
        Y2.append(alue[1])
plt.plot(X2, Y2, label='mss')

filename3 = 'C:/Users/fyq/OneDrive/桌面/实验dat文件/实验3/mprofile372.dat'                       # 文件名
X3,Y3 = [],[]
with open(filename3, 'r') as f:#1
    lines = f.readlines()#2
    for line in lines:#3
        lue = [float(s) for s in line.split()]#4
        X3.append(lue[0])#5
        Y3.append(lue[1])
plt.plot(X3, Y3, label='dns')

filename4 = 'C:/Users/fyq/OneDrive/桌面/实验dat文件/实验3/mprofile424.dat'                       # 文件名
X4,Y4 = [],[]
with open(filename4, 'r') as f:#1
    lines = f.readlines()#2
    for line in lines:#3
        ue = [float(s) for s in line.split()]#4
        X4.append(ue[0])#5
        Y4.append(ue[1])
plt.plot(X4, Y4, label='bfs')
# 添加图例
# https://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend
plt.legend(loc='upper left', frameon=True)
# 添加网格线
plt.grid(axis='both')
# 设置坐标名称
plt.xlabel("运行时间(s)", fontproperties=zhfont1)
plt.ylabel("运行内存(MiB)", fontproperties=zhfont1)
savefig("C:/Users/fyq/OneDrive/桌面/3.jpg")
plt.show()