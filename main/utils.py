import re
import networkx as nx
import sys
import ipdb
##### create graph
def create_g(filename):
    G = nx.DiGraph()
    with open(filename,encoding = 'utf-8') as file:
        lines = file.readlines()
        lines = [line.rstrip() for line in lines]

    nodetext = [node for node in lines if 'Mention' in node]
    nodes = []
    for i in range(len(nodetext)):
        # nodename = re.search('\{(.*)\}', nodetext[i])
        nodename = re.search('\'(.*)\'', nodetext[i])
        nodes.append((i, nodename.group(1)))
        G.add_node(i, name=nodename.group(1))

    edgetext = [edge for edge in lines if 'Action' in edge]
    edges = []
    for i in range(len(edgetext)):
        connect = re.findall('[0-9]+', edgetext[i])
        edgename = re.search('\:(.*)\{', edgetext[i]).group(1)
        edgeattr = re.search('\'(.*)\'', edgetext[i])
        if edgeattr == None:
            edgeattr = re.search('\"(.*)\"', edgetext[i])
        edgeattr = edgeattr.group(1)
        # edges.append((int(connect[0]), int(connect[1]), {edgename: edgeattr}))
        edges.append((int(connect[0]), int(connect[1]), edgeattr))
        G.add_edge(int(connect[0]), int(connect[1]), label=edgeattr)
    return G, nodes, edges

##### add weights from designed metric
def weights_measure(G, edges, nodes, source, target, metric='centrality'):
    if metric == 'centrality':
        scores = nx.betweenness_centrality(G)
        copy = G.copy()
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                all_path = list(nx.all_simple_paths(copy, i, j))
        weights = []
        for edge in edges:
            node0 = edge[0]
            node1 = edge[1]
            score = scores[node0]+scores[node1]
            if score == 0:
                weight = sys.MAX
            else: 
                weight = 2/score
            weights.append((node0, node1, weight))
        G.add_weighted_edges_from(weights)
    if metric == 'centrality':
        copy = G.copy()
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                all_path = nx.all_simple_paths(G, i, j)
    return G, weights

def find_all_path(G):
    all = []
    for i in range(len(G.nodes)):
        for j in range(len(G.nodes)):
            try:
                all_path = list(nx.all_simple_paths(G, i, j))
                all = all+all_path
            except (nx.exception.NetworkXNoPath): continue
    return all

def dupli(the_list):
    count = the_list.count # this optimization added courtesy of Sven's comment
    result = [(item, count(item)) for item in set(the_list)]
    result.sort()
    return result

def print_path_detail(G, p_list):
    for p in p_list:
        for i in range(len(p)-1):
            nodename = G.nodes[p[i]]['name']
            
            try: edgelabel = G.get_edge_data(p[i], p[i+1])['label']
            except(TypeError): ipdb.set_trace()
            print(nodename, edgelabel, '\u2192')
        print(G.nodes[p[-1]]['name'], '\n')