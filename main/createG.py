import networkx as nx
import re
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#创建节点
#with open("印度node.txt", "r",encoding="utf-8") as f:  # 打开文件
#    lines = f.readlines()  # 读取文件
#for line in lines:
#    id=line.split(',')[0]
#    name='mention='+line.split(',')[1].split('\n')[0]
#    node=str((id,name))
#    nodes.append(eval(node))
#G.add_nodes_from(nodes)
#创建边
#with open("印度edge.txt", "r",encoding="utf-8") as f:  # 打开文件
#    edgelines = f.readlines()  # 读取文件
#for line in edgelines:
#    edgename = 'name='+str(re.findall(r"Action:(.+?)}", str(line)))
#    edgename1=edgename.replace('["','').replace('"]','')#边的名称：使得、接着、子事件
#    x1=line.split(')')[0].split('(')[1]#头节点
#    x2=line.split(')')[1].split('(')[1]#尾节点
#    edge=str((x1,x2))
#    edges.append(eval(edge))
#G.add_edges_from(edges)
#创建有向图谱
G = nx.MultiDiGraph()
G.add_node(3,mention='军事政策推行')
G.add_node(4,mention='外交方针确定')
G.add_node(5,mention='国家矛盾增大')
G.add_node(6,mention='联训联演开展')
G.add_node(7,mention='作战能力提高')
G.add_node(8,mention='协作能力提高')
G.add_node(9,mention='协议共识达成')
G.add_node(10,mention='兵力装备调配')
G.add_node(11,mention='敌对势力相抗')
G.add_node(12,mention='军事打击行动开展')
G.add_node(13,mention='冲突双方主动寻求和解')
G.add_node(14,mention='冲突交火发生')
G.add_node(15,mention='冲突致使民众伤亡')
G.add_node(16,mention='军事威胁炒作渲染')
G.add_node(17,mention='武器装备具有优势')
G.add_node(18,mention='武器装备采购')
G.add_node(19,mention='制裁施压实施')
G.add_node(20,mention='军事演习开展')
G.add_node(21,mention='军事演习检验作战能力')
G.add_node(22,mention='新式武器试验研究')
G.add_node(25,mention='军事力量强化')
G.add_node(26,mention='地区存在加强')
G.add_node(27,mention='地区竞争胶着')
G.add_node(28,mention='军队体制改革')
G.add_node(29,mention='冲突对抗加剧')
G.add_node(30,mention='地区紧张局势加剧')
G.add_node(31,mention='外交会谈举行')
G.add_node(32,mention='地区局势向着和平发展')
G.add_node(33,mention='意见看法达成一致')
G.add_node(34,mention='地区紧张局势缓解')
G.add_node(35,mention='军事同盟关系展示')
G.add_node(36,mention='国家或势力设法遏制对手')
G.add_node(37,mention='军事设施建设')
G.add_node(38,mention='导弹系统试射')
G.add_node(39,mention='武器试射检验性能')
G.add_node(40,mention='国家或势力进行舆论攻击')
G.add_node(41,mention='武器研发能力不足导致问题')
G.add_node(42,mention='反制行动实施')
G.add_node(43,mention='战略威慑力提升')
G.add_node(44,mention='巡航行动开展')
G.add_node(45,mention='军事立场申明')
print(G.nodes)

G.add_edges_from([(5,11),(14,5),(22,7),(6,7),(31,13),(6,8),(4,9),(16,22),(20,21),(5,4),(3,4),(5,16),(17,18),(18,26),(35,31),(31,36),(31,30),(8,16),(7,16),(8,28),(7,28),(27,26),(27,11),(27,3),(3,20),
(20,30),(20,8),(10,7),(7,26),(29,30),(33,34),(13,34),(38,7),(38,39),(42,18),(20,7),(3,18),(10,4),(20,4),(6,4),(10,37),(8,9),(43,18),(41,18),(4,18),(3,44),(11,20),(6,11),(3,11),(4,44),(3,9),(36,9),(16,30)],attribute="使得")
G.add_edges_from([(16,22),(22,7),(16,4),(20,10),(10,6),(26,10),(9,18),(18,19),(19,3),(5,6),(11,10),(6,20),(6,12),(12,13),(13,14),(3,35),(5,29),(29,9),(31,32),(32,33),(33,13),
(34,31),(31,37),(31,13),(37,20),(20,16),(4,9),(16,18),(22,41),(18,22),(22,25),(11,18),(18,6),(6,44),(44,45),(19,31),(31,3),(18,31),(31,10),(18,38),(38,22),(22,40),(40,18),(10,3),(4,31)], attribute="接着")
G.add_edges_from([(36,11),(14,5),(15,14),(33,31),(8,26)],attribute="子事件")
print(G.edges)

#显示边或节点的属性
name = nx.get_edge_attributes(G, 'attribute')
print(name)
nx.draw_networkx(G, with_labels=True)
plt.show()