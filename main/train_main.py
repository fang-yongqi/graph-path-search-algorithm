import networkx as nx
import pickle
import numpy as np
import re
import matplotlib.pyplot as plt
from utils import *
import ipdb
import datetime
start = datetime.datetime.now()
# 文件
filename = '印度事理.txt'
# 设置参数，E1为最后输出路径集
k =5
source =7
target = 35
E1 = []
metric = 'connectivity'
# 创建图谱
G, nodes, edges = create_g(filename)
# G_weigh, weights = weights_measure(G, edges, source, target, metric='centrality')

# all_path存储所有路径，如果两点之间没有路径则print
try: 
    all_path = list(nx.all_simple_paths(G, source, target))
except (nx.exception.NetworkXNoPath): 
    print('节点对之间无连同路径')
# influence1~4分别存储四个价值函数的相应得分
influence1 = []
influence2 = []
influence3 = []
influence4 = []
# score1计算并存储图谱G中的所有节点的介性中心度分数
scores1 = nx.betweenness_centrality(G)
# score2计算并存储图谱G中的所有节点的关注度分数
attention = [3.1, 3.6, 4.7, 3.5, 4.6, 4.9, 3.5, 4.9, 4.5, 3.8, 4.3, 4.6, 4.6, 4.9, 4.1, 4.2, 
3.1, 3.4, 3.9, 4.4, 4.6, 4.5, 5.0, 4.6, 4.8, 4.4, 3.8, 3.7, 3.3, 4.1, 4.6, 4.8, 
4.9, 4.3, 3.1, 3.9, 4.6, 4.5, 4.2, 3.7, 3.1, 4.4, 4.1, 4.0, 3.9, 4.8, 3.0, 3.2]
scores2 = [0] * len(nodes)
for i in range(len(nodes)):
    pre = [pred for pred in G.predecessors(i)]
    if not pre:
        scores2[i] = G.degree[i] * 0.2
    else: 
        scores2[i] = sum(attention[j]/G.degree[j] for j in pre)*0.8 + G.degree[i] * 0.2
# connect是计算score3的参数
connect = [0]*3
#以下均为计算score4需要的参数
all = find_all_path(G)
alllen = [len(p) for p in all]
allnum = list(set(alllen))
allcount = [0] * max(allnum)
for i in range(1, len(allcount)+1):
    allcount[i-1] = alllen.count(i)
plen = [len(p) for p in all_path]
pnum = list(set(plen))
pcount = [0] * max(pnum)
for i in range(1, len(pcount)+1):
    pcount[i-1] = plen.count(i)

# 对于路径，分别用influence1~4进行打分
for p in all_path:
    influence1.append(sum([scores1[i] for i in p]))
    influence2.append(sum([scores2[i] for i in p]))
    for i in range(len(p)-1):
        label = G.get_edge_data(p[i], p[i+1])['label']
        if label == '使得': connect[0] += 1
        elif label == '接着': connect[1] += 1
        else: connect[2] += 1
    influence3.append(sum([a*b for a,b in zip(connect, [0.6, 0.3, 0.1])])*0.8)
    tmp = len(p)-1
    influence4.append(np.log(1+allcount[tmp]/pcount[tmp]))
    
# 寻找最具价值路径：influence为路径的最终总得分，max_ind是取其中的最高得分，将最高得分路径添加到E1的末尾
influence = [a*b*c*d for a,b,c,d in zip(influence1,influence2,influence3,influence4)]
max_ind = np.argmax(influence)
E1.append(all_path[max_ind])
end = datetime.datetime.now()
# 寻找次最具价值路径
cur_base = 0
# 执行while直到输出路径达到k条
while (len(E1) < k):
    cur_base_path = E1[cur_base]
    E2 = []
# 以第一条路径为偏离路径，依次删除各个边，并在对应的子图中搜索出相应的最具价值路径
    for i in range(len(cur_base_path)-1):
        gcopy = G.copy()
        gcopy.remove_edge(cur_base_path[i], cur_base_path[i+1])
        try: 
            part_path = list(nx.all_simple_paths(gcopy, cur_base_path[i], cur_base_path[i+1]))
        except (nx.exception.NetworkXNoPath): continue
        if not part_path: continue

        plen = [len(p) for p in part_path]
        pnum = list(set(plen))
        pcount = [0] * max(pnum)
        for j in range(1, len(pcount)+1):
            pcount[j-1] = plen.count(j)
    
        influence1 = []
        influence2 = []
        influence3 = []
        influence4 = []

        for p in part_path:
            influence1.append(sum([scores1[i] for i in p]))
            influence2.append(sum([scores2[i] for i in p]))
            for j in range(len(p)-1):
                label = G.get_edge_data(p[j], p[j+1])['label']
                if label == '使得': connect[0] += 1
                elif label == '接着': connect[1] += 1
                else: connect[2] += 1
            influence3.append(sum([a*b for a,b in zip(connect, [0.6, 0.3, 0.1])])*0.8)
            tmp = len(p)-1
            # ipdb.set_trace()
            influence4.append(np.log(1+allcount[tmp]/pcount[tmp]))
        influence = [a*b*c*d for a,b,c,d in zip(influence1,influence2,influence3,influence4)]
        max_ind = np.argmax(influence)
        # ipdb.set_trace()
        if i == len(cur_base_path)-2:
            new_path = cur_base_path[:i] + p
        else:
            new_path = cur_base_path[:i] + p + cur_base_path[i+2:]
        E2.append(new_path)
    E1 = E1+E2
    if len(E1)-1 > cur_base:
        cur_base = cur_base+1
    else:
        print('There is only {} valid paths between source node and target node'.format(len(E1)))
        print_path_detail(G, E1)

print(E1[:k])
print_path_detail(G, E1[:k])
print (end-start)