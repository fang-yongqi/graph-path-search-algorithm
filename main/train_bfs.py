import networkx as nx
import pickle
import numpy as np
import ipdb
import re
import matplotlib.pyplot as plt
import sys
from utils import *
import datetime
start = datetime.datetime.now()
# 文件
filename = '印度事理.txt'
# 参数
k = 5
source = 7
target = 35
E1 = []
metric = 'connectivity'
# 创建图谱
G, nodes, edges = create_g(filename)
# G_weigh, weights = weights_measure(G, edges, source, target, metric='centrality')

# 定义每种价值函数
try: 
    all_path = list(nx.all_simple_paths(G, source, target))
except (nx.exception.NetworkXNoPath): 
    print('invalid input of source node and target node')

influence1 = []
influence2 = []
influence3 = []
influence4 = []
# 函数1
scores1 = nx.betweenness_centrality(G)
# 函数2
attention = [3.1, 3.6, 4.7, 3.5, 4.6, 4.9, 3.5, 4.9, 4.5, 3.8, 4.3, 4.6, 4.6, 4.9, 4.1, 4.2, 
3.1, 3.4, 3.9, 4.4, 4.6, 4.5, 5.0, 4.6, 4.8, 4.4, 3.8, 3.7, 3.3, 4.1, 4.6, 4.8, 
4.9, 4.3, 3.1, 3.9, 4.6, 4.5, 4.2, 3.7, 3.1, 4.4, 4.1, 4.0, 3.9, 4.8, 3.0, 3.2]
scores2 = [0] * len(nodes)
for i in range(len(nodes)):
    pre = [pred for pred in G.predecessors(i)]
    if not pre:
        scores2[i] = G.degree[i] * 0.2
    else: 
        scores2[i] = sum(attention[j]/G.degree[j] for j in pre)*0.8 + G.degree[i] * 0.2

connect = [0]*3

all = find_all_path(G)
alllen = [len(p) for p in all]
allnum = list(set(alllen))
allcount = [0] * max(allnum)
for i in range(1, len(allcount)+1):
    allcount[i-1] = alllen.count(i)
plen = [len(p) for p in all_path]
pnum = list(set(plen))
pcount = [0] * max(pnum)
for i in range(1, len(pcount)+1):
    pcount[i-1] = plen.count(i)
end = datetime.datetime.now()
# bfs搜索
bfsnew = dict(nx.bfs_predecessors(G, source))
if target not in bfsnew:
    print('invalid input of source node and target node')
final = target
first_path = []
first_path.append(final)
while final != source:
    value = bfsnew[final]
    first_path.append(value)
    del bfsnew[final]
    final = value
first_path = list(reversed(first_path))
E1.append(first_path)

#继续搜索next路径
cur_base = 0
while (len(E1) < k):
    cur_base_path = E1[cur_base]
    E2 = []
    for i in range(len(cur_base_path)-1):
        gcopy = G.copy()
        gcopy.remove_edge(cur_base_path[i], cur_base_path[i+1])
        try: 
            bfsnew = dict(nx.bfs_predecessors(gcopy, cur_base_path[i]))
            if cur_base_path[i+1] not in bfsnew: continue
            final = cur_base_path[i+1]
            part_path = [final]
            while final != cur_base_path[i]:
                value = bfsnew[final]
                part_path.append(bfsnew[final])
                del bfsnew[final]
                final = value
            part_path = list(reversed(part_path))

        except (nx.exception.NetworkXNoPath): continue
        
        if i == len(cur_base_path)-2:
            new_path = cur_base_path[:i] + part_path
        else:
            new_path = cur_base_path[:i] + part_path + cur_base_path[i+2:]
        if new_path.count(target) > 1:
            new_path = new_path[:new_path.index(target)+1]
        if len(new_path) >= len(allcount): continue
        E2.append(new_path)

    if not E2:
        print('There is only {} valid paths between source node and target node'.format(len(E1)))
        print_path_detail(G, E1)
        exit()
    plen = [len(p) for p in E2]
    pnum = list(set(plen))
    pcount = [0] * max(pnum)
    for i in range(1, len(pcount)+1):
        pcount[i-1] = plen.count(i)

    influence1 = []
    influence2 = []
    influence3 = []
    influence4 = []

    for p in E2:
        influence1.append(sum([scores1[i] for i in p]))
        influence2.append(sum([scores2[i] for i in p]))
        for i in range(len(p)-1):
            label = G.get_edge_data(p[i], p[i+1])['label']
            if label == '使得': connect[0] += 1
            elif label == '接着': connect[1] += 1
            else: connect[2] += 1
        influence3.append(sum([a*b for a,b in zip(connect, [0.6, 0.3, 0.1])])*0.8)
        tmp = len(p)-1
        influence4.append(np.log(1+allcount[tmp]/pcount[tmp]))

    influence = [a*b*c*d for a,b,c,d in zip(influence1,influence2,influence3,influence4)]
    max_ind = np.argmax(influence)
    # ipdb.set_trace()
    E1 = E1+E2
    if len(E1)-1 > cur_base:
        cur_base = cur_base+1
    else:
        print('There is only {} valid paths between source node and target node'.format(len(E1)))
        print(E1)
        exit()
print(E1[:k])
print_path_detail(G, E1[:k])
print (end-start)


